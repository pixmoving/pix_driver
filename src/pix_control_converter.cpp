// Copyright PIXMOVING, INC.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Co-developed by PIXMOVING, INC.

#include "pix_control_converter.hpp"

using namespace std::chrono_literals;

namespace autoware
{
namespace drivers
{
namespace pix_driver
{

PixControlConverter::PixControlConverter() : Node("pix_control_converter"), count_(0)
{
    pub_pix_control_ = this->create_publisher<pix_driver_msg::msg::PixloopControl>("pix_control_cmd", 10);
    timer_ = this->create_wall_timer(20ms, std::bind(&PixControlConverter::control_publish, this));

    sub_vehicle_command_ = this->create_subscription<autoware_auto_msgs::msg::VehicleControlCommand>(
        "vehicle_control_command", 10, std::bind(&PixControlConverter::vehicle_command_callback, this, std::placeholders::_1)
    );
    sub_vehicle_state_ = this->create_subscription<autoware_auto_msgs::msg::VehicleStateCommand>(
        "vehicle_state_command", 10, std::bind(&PixControlConverter::vehicle_state_callback, this, std::placeholders::_1)
    );
}

        
void PixControlConverter::vehicle_state_callback(const autoware_auto_msgs::msg::VehicleStateCommand::SharedPtr msg)
{
    vehicle_state_ptr = msg;
    RCLCPP_INFO(this->get_logger(),"Publishing: '%i'", vehicle_state_ptr->mode);
}

void PixControlConverter::vehicle_command_callback(const autoware_auto_msgs::msg::VehicleControlCommand::SharedPtr msg)
{
    vehicle_command_ptr = msg;
}

void PixControlConverter::control_publish()
{
    auto message = pix_driver_msg::msg::PixloopControl();

    // decode code below

    /*----------------------------*/
    // vehicle state command

    // blinker
    switch(vehicle_state_ptr->blinker) // blinker
    {
        case 0: // no command
            message.left_light = false;
            message.right_light = false;
            break;
        case 1: // off
            message.left_light = false;
            message.right_light = false;
            break;
        case 2: // left on
            message.left_light = true;
            message.right_light = false;
            break;
        case 3: // right on
            message.left_light = false;
            message.right_light = true;
            break;
        case 4: // hazard
            message.left_light = true;
            message.right_light = true;
            break;
        default:
            message.left_light = false;
            message.right_light = false;
            break;

    }
    // headlight
    switch(vehicle_state_ptr->headlight)
    {
        case 0: // no command
            message.front_light = false;
            break;
        case 1: // off
            message.front_light = false;
            break;
        case 2: // on
            message.front_light = true;
            break;
        case 3: // high
            message.front_light = true;
            break;
        default:
            message.front_light = false;
            break;
    }

    // wiper
    /*
    code for wiper
    */

    // gear
    switch(vehicle_state_ptr->gear)
    {
        case 0: // no command
            message.gear_shift = 2;
            break;
        case 1: // drive
            message.gear_shift = 1;
            break;
        case 2: // reverse
            message.gear_shift = 3; 
            break;
        case 3: // park
            message.gear_shift = 1;
            break;
        case 4: // low
            message.gear_shift = 1;
            break;
        case 5: // neutual
            message.gear_shift = 2;
            break;
        default:
            message.gear_shift = 2;
            break;
    }

    // autonomous enable
    switch(vehicle_state_ptr->mode)
    {
        case 0: // no command
            message.self_driving_enable = false;
            break;
        case 1: // autonomous mode
            message.self_driving_enable = true;
            break;
        case 2: // manual mode
            message.self_driving_enable = false;
            break;
        default:
            message.self_driving_enable = false;
            break;
    }

    // hand brake
    if(vehicle_state_ptr->hand_brake)
    {
        message.epb = true;
    }
    else
    {
        message.epb = false;
    }

    // horn
    /*
    code for horn
    */

    /*----------------------------*/
    // vehicle control command
    
    // long accel mps2
    /*
    code for long accel mps2. The code is void because there is no interace for accel control in Pixloop chassis
    */

    // velocity mps
    message.speed = vehicle_command_ptr->velocity_mps * 36;

    // front wheel angle in radias
    message.steering = -((vehicle_command_ptr->front_wheel_angle_rad / 3.1415926)*180) * (1023/26.0);

    // rear wheel angle in radias
    /*
    code for read wheel angle in radias
    */

    // publish message
    pub_pix_control_->publish(message);

}

} // pix_driver
} // drivers
} // autoware

// RCLCPP_COMPONENTS_REGISTER_NODE(autoware::drivers::pix_driver_ros2::PixDriverSubscriber)
int main(int argc, char * argv[])
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<autoware::drivers::pix_driver::PixControlConverter>());
    rclcpp::shutdown();
    return 0;
}
