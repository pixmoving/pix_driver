// Copyright PIXMOVING, INC.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Co-developed by PIXMOVING, INC.

#include "pix_feedback_converter.hpp"

using namespace std::chrono_literals;

namespace autoware
{
namespace drivers
{
namespace pix_driver
{

PixFeedbackConverter::PixFeedbackConverter() : Node("pix_feedback_converter")
{
    pub_vehicle_odom_ = this->create_publisher<autoware_auto_msgs::msg::VehicleOdometry>("vehicle_odometry", 10);
    pub_state_ = this->create_publisher<autoware_auto_msgs::msg::VehicleStateReport>("vehicle_state_report", 10);

    sub_pix_feedback_ = this->create_subscription<pix_driver_msg::msg::PixloopFeedback>(
        "pixloop_feedback", 10, std::bind(&PixFeedbackConverter::pix_feedback_callback, this, std::placeholders::_1)
    );
}

        
void PixFeedbackConverter::pix_feedback_callback(const pix_driver_msg::msg::PixloopFeedback::SharedPtr msg)
{

    // odom and state_report message of autoware_auto_msgs initialization
    auto odom_msg = autoware_auto_msgs::msg::VehicleOdometry();
    auto state_msg = autoware_auto_msgs::msg::VehicleStateReport();

    rclcpp::Clock::SharedPtr clock = std::make_shared<rclcpp::Clock>(RCL_ROS_TIME);

    // odometry message converting
    odom_msg.stamp = clock->now();

    // velocity mps
    odom_msg.velocity_mps = static_cast<float>(msg->speed_feedback/36.0f);

    // front wheel angle rads
    odom_msg.front_wheel_angle_rad = -static_cast<float>(msg->f_steer_feedback-700) * (MAXSTEER/150.0f);

    // vehicle state message coverting
    state_msg.stamp = clock->now();

    // fuel or battery remain
    state_msg.fuel = 50;

    // blinker
    bool r_steering_light, l_steering_light;
    r_steering_light = msg->r_steer_light_feedback;
    l_steering_light = msg->l_steer_light_feedback;
    if(r_steering_light==true&&l_steering_light==true)
    {
        state_msg.blinker = 4;
    }
    if(r_steering_light==true&&l_steering_light==false)
    {
        state_msg.blinker = 3;
    }
    if(r_steering_light==false&&l_steering_light==true)
    {
        state_msg.blinker = 2;
    }
    if(r_steering_light==false&&l_steering_light==false)
    {
        state_msg.blinker = 1;
    }

    // headlight
    if(msg->tail_light_feedback)
    {
        state_msg.headlight = 3;
    }
    else if(msg->tail_light_feedback==false)
    {
        state_msg.headlight = 2;
    }
    else
    {
        state_msg.headlight = 1;
    }

    // wiper
    state_msg.wiper = false;

    // gear
    switch (msg->gear_feedback)
    {
    case 1:
        state_msg.gear = 1;
        break;
    case 2:
        state_msg.gear = 5;
        break;
    case 3:
        state_msg.gear = 2;
        break;
    default:
        state_msg.gear = 5;
        break;
    }

    // self-driving mode state
    if(msg->vehicle_mode_feedback)
    {
        state_msg.mode = 1;
    }
    else
    {
        state_msg.mode = 2;
    }

    // hand brake
    state_msg.hand_brake = false;

    // horn
    state_msg.horn = false;

    // publish all msgs
    pub_vehicle_odom_->publish(odom_msg);
    pub_state_->publish(state_msg);

}

} // pix_driver
} // drivers
} // autoware

// RCLCPP_COMPONENTS_REGISTER_NODE(autoware::drivers::pix_driver_ros2::PixDriverSubscriber)
int main(int argc, char * argv[])
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<autoware::drivers::pix_driver::PixFeedbackConverter>());
    rclcpp::shutdown();
    return 0;
}
