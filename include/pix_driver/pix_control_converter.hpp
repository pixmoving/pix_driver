// Copyright PIXMOVING, INC.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Co-developed by PIXMOVING, INC.

#ifndef PIX_DRIVER__PIX_CONTROL_CONVERTER_HPP_
#define PIX_DRIVER__PIX_CONTROL_CONVERTER_HPP_

#include <iostream>
#include <string>
#include <memory>
#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "pix_driver_msg/msg/pixloop_control.hpp"
#include "autoware_auto_msgs/msg/vehicle_control_command.hpp"
#include "autoware_auto_msgs/msg/vehicle_state_command.hpp"

namespace autoware
{
namespace drivers
{
namespace pix_driver
{

class PixControlConverter : public rclcpp::Node
{
    public:
        PixControlConverter();
    
    private:
        autoware_auto_msgs::msg::VehicleControlCommand::SharedPtr vehicle_command_ptr;
        autoware_auto_msgs::msg::VehicleStateCommand::SharedPtr vehicle_state_ptr;

        void control_publish();
        void vehicle_command_callback(const autoware_auto_msgs::msg::VehicleControlCommand::SharedPtr msg);
        void vehicle_state_callback(const autoware_auto_msgs::msg::VehicleStateCommand::SharedPtr msg);

        rclcpp::Publisher<pix_driver_msg::msg::PixloopControl>::SharedPtr pub_pix_control_;
        rclcpp::Subscription<autoware_auto_msgs::msg::VehicleControlCommand>::SharedPtr sub_vehicle_command_;
        rclcpp::Subscription<autoware_auto_msgs::msg::VehicleStateCommand>::SharedPtr sub_vehicle_state_;

        rclcpp::TimerBase::SharedPtr timer_;
        size_t count_;

}; // class PixControlConverter

} // namespace pix_driver
} // namespace drivers
} // namespace autoware


#endif // PIX_DRIVER__PIX_CONTROL_CONVERTER_HPP_