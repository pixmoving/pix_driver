// Copyright PIXMOVING, INC.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Co-developed by PIXMOVING, INC.

#ifndef PIX_DRIVER__PIX_FEEDBACK_CONVERTER_HPP_
#define PIX_DRIVER__PIX_FEEDBACK_CONVERTER_HPP_
#include <iostream>
#include <string>
#include <memory>
#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "rclcpp/clock.hpp"
#include "rclcpp/time_source.hpp"
#include "pix_driver_msg/msg/pixloop_feedback.hpp"
#include "autoware_auto_msgs/msg/vehicle_odometry.hpp"
#include "autoware_auto_msgs/msg/vehicle_state_report.hpp"
#include "autoware_auto_msgs/msg/vehicle_kinematic_state.hpp"

namespace autoware
{
namespace drivers
{
namespace pix_driver
{

class PixFeedbackConverter : public rclcpp::Node
{
    public:
        PixFeedbackConverter();
    
    private:
        void pix_feedback_callback(const pix_driver_msg::msg::PixloopFeedback::SharedPtr msg);
        rclcpp::Subscription<pix_driver_msg::msg::PixloopFeedback>::SharedPtr sub_pix_feedback_;
        rclcpp::Publisher<autoware_auto_msgs::msg::VehicleOdometry>::SharedPtr pub_vehicle_odom_;
        rclcpp::Publisher<autoware_auto_msgs::msg::VehicleStateReport>::SharedPtr pub_state_;

        const float MAXSTEER = 30;

}; // class PixFeedbackConverter

} // namespace pix_driver
} // namespace drivers
} // namespace autoware


#endif // PIX_DRIVER__PIX_FEEDBACK_CONVERTER_HPP_