# pix_driver

PIXLOOP is the world’s first smart chassis with open source software and hardware, enabling rapid as well as convenient customization on your autonomous driving products.

PIXLOOP is dedicated to enhancing the innovation in autonomous driving applications and to greatly reducing the lead time for product development.

Adopting software-defined modular design, PIXLOOP provides an open source solution that is reliable, robust, extensible, easy-to-use, high-performance, and cost-effective, specifically targeted for industries, academia and developer communities.  

We we develop this ros2 driver in order to enable the usage of Autoware.auto on PIXLOOP chassis.
[PIXLOOP CHASSIS](https://gitlab.com/pixmoving/pixbot)
